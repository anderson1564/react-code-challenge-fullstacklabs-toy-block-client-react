import fetch from 'cross-fetch';
import * as types from '../constants/actionTypes';

const checkNodeStatusStart = (node) => {
  return {
    type: types.CHECK_NODE_STATUS_START,
    node
  };
};

const checkNodeStatusSuccess = (node, res) => {
  return {
    type: types.CHECK_NODE_STATUS_SUCCESS,
    node,
    res
  };
};

const checkNodeStatusFailure = node => {
  return {
    type: types.CHECK_NODE_STATUS_FAILURE,
    node,
  };
};

export function checkNodeStatus(node) {
  return async (dispatch) => {
    try {
      dispatch(checkNodeStatusStart(node));
      const res = await fetch(`${node.url}/api/v1/status`);

      if(res.status >= 400) {
        dispatch(checkNodeStatusFailure(node));
      }

      const json = await res.json();

      dispatch(checkNodeStatusSuccess(node, json));
    } catch (err) {
      dispatch(checkNodeStatusFailure(node));
    }
  };
}

export function checkNodeStatuses(list) {
  return (dispatch) => {
    list.forEach(node => {
      dispatch(checkNodeStatus(node));
    });
  };
}


const checkNodeBlocksStatusStart = (url) => {
  return {
    type: types.CHECK_NODE_BLOCKS_STATUS_START,
    url
  };
};

const checkNodeBlocksStatusSuccess = (url, blocks) => {
  return {
    type: types.CHECK_NODE_BLOCKS_STATUS_SUCCESS,
    url,
    blocks
  };
};

const checkNodeBlocksStatusFailure = url => {
  return {
    type: types.CHECK_NODE_BLOCKS_STATUS_FAILURE,
    url,
  };
};

export function checkNodeBlocksStatus(url) {
  return async (dispatch) => {
    try {
      dispatch(checkNodeBlocksStatusStart(url));
      const res = await fetch(`${url}/api/v1/blocks`);

      
      if(res.status >= 400) {
        dispatch(checkNodeBlocksStatusFailure(url));
      }

      const json = await res.json();

      dispatch(checkNodeBlocksStatusSuccess(url, json));
    } catch (err) {
      dispatch(checkNodeBlocksStatusFailure(url));
    }
  };
}

export function checkNodeBlocksStatuses(list) {
  return (dispatch) => {
    list.forEach(node => {
      dispatch(checkNodeBlocksStatus(node.url));
    });
  };
}