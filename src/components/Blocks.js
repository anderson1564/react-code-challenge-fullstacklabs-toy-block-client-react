import React from "react";
import PropTypes from "prop-types";
import {Typography, makeStyles, Grid} from "@material-ui/core";
import CircularProgress from '@material-ui/core/CircularProgress';


import Block from './Block';

const useStyles = makeStyles((theme) => ({
    root: {
        margin: theme.spacing(1, 0),
        width: '100%',
        overflowX: 'hidden',
    },
}))

function Blocks({blocks}) {
    const classes = useStyles();

    if(!blocks) return false;

    const { error, loading, data } = blocks;

    if(error) {
        return (
            <div className={classes.root}>
                <Typography align="center" color="error">Error</Typography>
            </div>
        );
    }

    if(loading) {
        return (
            <div className={classes.root}>
                <Typography align="center">Loading</Typography>
                <CircularProgress />
            </div>
        );
    }

    return (
        <div className={classes.root}>
            <Grid container spacing={1}>
                {data.map(({id, attributes}) => {
                    const { data, index } = attributes
                    return (
                        <Grid item xs={12}>
                            <Block key={id} data={data} index={index} />
                        </Grid>
                    );
                })}
            </Grid>
        </div>
    );
}


Blocks.propTypes = {
    blocks: PropTypes.shape({
        loading: PropTypes.bool.isRequired,
        error: PropTypes.bool.isRequired,
        data: PropTypes.arrayOf(
            PropTypes.shape({
                id: PropTypes.string.isRequired,
                attributes: PropTypes.shape({
                    index: PropTypes.number.isRequired,
                    data: PropTypes.string.isRequired,
                }).isRequired,
            }).isRequired,
        )
    })
}

export default Blocks;