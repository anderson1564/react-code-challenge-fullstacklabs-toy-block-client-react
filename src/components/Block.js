import React from "react";
import PropTypes from "prop-types";
import {
  Typography,
  makeStyles,
} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
    root: {
        background: 'rgba(0, 0, 0, 0.12)',
        borderRadius: '2px',
        padding: theme.spacing(1),
        width: '100%'
    },
    index: {
        fontSize: theme.typography.pxToRem(10),
        lineHeight: '16px',
        letterSpacing: '1.5px',
        textTransform: 'uppercase',
        color: '#304FFE',
    },
    data: {

    }
}))

function Block({data, index}) {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Typography classes={{root: classes.index}}>
                {index}
            </Typography>
            <Typography classes={{root: classes.data}}>
                {data}
            </Typography>
        </div>
    );
}

Block.propTypes= {
    index: PropTypes.number.isRequired,
    data: PropTypes.string.isRequired,
}

export default Block;